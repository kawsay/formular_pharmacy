class ApplicationController < ActionController::Base
  rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
    render :text => exception, :status => 500
  end

  # Escape the endless login loop by redicting it to home
  # See: https://www.rubydoc.info/github/plataformatec/devise/Devise%2FControllers%2FHelpers:after_sign_in_path_for
  def after_sign_in_path_for(resource)
    home_path
  end

  def admin?
    user_signed_in? && current_user.is_admin? == true
  end
  helper_method :admin?
end
