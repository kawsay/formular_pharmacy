module AccessControl
  extend ActiveSupport::Concern

  def restrict_access_to_administrators
    deny_access unless user_signed_in? && admin?
  end

  def restrict_access_to_connected_users
    deny_access unless user_signed_in?
  end

  def restrict_access_to_concerned_user
    deny_access unless user_signed_in? && is_related_to_current_user?
  end

  def restrict_access_to_administrators_or_concerned_user
    deny_access unless user_signed_in? && (is_related_to_current_user? || admin?)
  end

  private

  def deny_access
    flash[:danger] = "Vous n'avez pas l'autorisation d'accéder à cette page"
    redirect_to :root
  end

  def is_related_to_current_user?
    object = controller_name.to_sym
    UserOrder.where(user: current_user, object => params[:id]).exists?
  end
end
