class FormularOrdersController < ApplicationController
  include AccessControl

  before_action :set_formular_order, only: [:show, :edit, :update, :destroy, :modal]
  before_action :restrict_access_to_administrators_or_concerned_user

  # GET /formular_orders
  # GET /formular_orders.json
  def index
    # Use this later for tracking users's submissions
    # @formular_orders = FormularOrder.all
    
    @formulars = Formular.all
  end

  # GET /formular_orders/1
  # GET /formular_orders/1.json
  def show
    @formular_order = FormularOrder.includes(:user).find(formular_order_params[:id])
    @formular       = Formular.includes(formular_products: :product).find(formular_order_params[:formular_id])
  end

  # GET /formular_orders/new
  def new
    @formular_order = FormularOrder.new
  end

  # GET /formular_orders/1/edit
  def edit
  end

  # POST /formular_orders
  # POST /formular_orders.json
  def create
    @formular_order = FormularOrder.new(formular_order_params)

    respond_to do |format|
      if @formular_order.save
        format.js { flash.now[:success] = 'Soumission enregistrée' }
        format.html { redirect_to @formular_order, notice: 'Formular order was successfully created.' }
        format.json { render :show, status: :created, location: @formular_order }
      else
        fomart.js { flash.now[:danger] = @formular_order.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @formular_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /formular_orders/1
  # PATCH/PUT /formular_orders/1.json
  def update
    respond_to do |format|
      if @formular_order.update(formular_order_params)
        format.js { flash.now[:notice] = 'CHECK FormularOrder CONTROLLER' }
        format.html { redirect_to @formular_order, notice: 'Formular order was successfully updated.' }
        format.json { render :show, status: :ok, location: @formular_order }
      else
        format.js { flash.now[:danger] = @formular_order.errors.full_messages.first }
        format.html { render :edit }
        format.json { render json: @formular_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /formular_orders/1
  # DELETE /formular_orders/1.json
  def destroy
    @formular_order.destroy
    respond_to do |format|
      format.js { flash.now[:warning] = 'CHECK FormularOrder CONTROLLER' }
      format.html { redirect_to formular_orders_url, notice: 'Formular order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def modal
    @formular_order = FormularOrder.includes(order_products: :product).find(formular_order_params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_formular_order
      @formular_order = FormularOrder.find(params[:id])
    end

    def set_formular
      @formular = Formular.find(params[:formular_id])
    end

    # Only allow a list of trusted parameters through.
    def formular_order_params
      params.permit(:formular_id, :order_id, :has_product_categories, :id)
    end
end
