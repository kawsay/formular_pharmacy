class FormularProductsController < ApplicationController
  include AccessControl

  before_action :set_formular_product, only: [:show, :edit, :update, :destroy]
  before_action :restrict_access_to_administrators

  # GET /formular_products
  # GET /formular_products.json
  def index
    @formular_products = FormularProduct.all
  end

  # GET /formular_products/1
  # GET /formular_products/1.json
  def show
  end

  # GET /formular_products/new
  def new
    @formular_product = FormularProduct.new
  end

  # GET /formular_products/1/edit
  def edit
  end

  # POST /formular_products
  # POST /formular_products.json
  def create
    @formular_product = FormularProduct.new(formular_product_params)

    unless formular_product_params[:product_id] =~ /\A\d+\z/
      @formular_product.product_id = Product.find_by(name: formular_product_params[:product_id]).id
    end

    respond_to do |format|
      if @formular_product.save
        format.js { flash.now[:success] = 'Article ajouté au formulaire' }
        format.html { redirect_to @formular_product, notice: 'Article ajouté au formulaire' }
        format.json { render :show, status: :created, location: @formular_product }
      else
        format.js { flash.now[:danger] = @formular_product.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @formular_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # Unused
  #
  # # PATCH/PUT /formular_products/1
  # # PATCH/PUT /formular_products/1.json
  # def update
  #   respond_to do |format|
  #     if @formular_product.update(formular_product_params)
  #       format.html { redirect_to @formular_product, notice: 'Formular product was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @formular_product }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @formular_product.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /formular_products/1
  # DELETE /formular_products/1.json
  def destroy
    @formular_product.destroy
    respond_to do |format|
      format.js { flash.now[:success] = 'Article retiré de la catégorie' }
      format.html { redirect_to formular_products_url, notice: 'Formular product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_formular_product
      @formular_product = FormularProduct.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def formular_product_params
      params.require(:formular_product).permit(:formular_id, :product_id)
    end
end
