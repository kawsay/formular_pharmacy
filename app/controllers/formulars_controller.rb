class FormularsController < ApplicationController
  include AccessControl

  before_action :set_formular, only: [:edit, :update, :destroy]
  before_action :restrict_access_to_connected_users, only: :index
  before_action :restrict_access_to_administrators, only: [:show, :new, :edit,
                                                           :update, :destroy]

  # GET /formulars
  # GET /formulars.json
  def index
    #@order = Order.new
    @formulars = Formular.all
    @formular  = Formular.new
  end

  # GET /formulars/1
  # GET /formulars/1.json
  def show
    @formular = Formular.includes(formular_orders: :user).find(params[:id])
    @submissions       = @formular
    @products          = Product.all
                                .order(:name)
                                .pluck(:id, :name)
    @formular_product  = FormularProduct.new(formular_id: @formular.id)
    @formular_products = @formular.formular_products
                                  .order(created_at: :desc)
                                  .includes(:product)
                                  .map(&:product)
  end

  # GET /formulars/new
  def new
    @formular = Formular.new
  end

  # GET /formulars/1/edit
  def edit
  end

  # POST /formulars
  # POST /formulars.json
  def create
    @formular = Formular.new(formular_params)

    @formular.write_attribute(:has_product_categories, true)         if params.dig(:formular, :has_product_categories) == '1'
    @formular.write_attribute(:products_have_dosage, true)           if params.dig(:formular, :products_have_dosage) == '1'
    @formular.write_attribute(:products_have_maximum_quantity, true) if params.dig(:formular, :products_have_maximum_quantity) == '1'
    @formular.write_attribute(:products_have_minimum_quantity, true) if params.dig(:formular, :products_have_minimum_quantity) == '1'
    @formular.write_attribute(:products_have_previous_batch, true)   if params.dig(:formular, :products_have_previous_batch) == '1'

    respond_to do |format|
      if @formular.save
        @formular_product  = FormularProduct.new(formular_id: @formular.id)
        @products          = Product.all.pluck(:id, :name)
        @formular_products ||= []

        format.js { flash.now[:success] = 'Formulaire créé' }
        format.html { redirect_to @formular, notice: 'Formular was successfully created.' }
        format.json { render :show, status: :created, location: @formular }
      else
        format.js { flash.now[:danger] = @formular.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @formular.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /formulars/1
  # PATCH/PUT /formulars/1.json
  def update
    respond_to do |format|
      if @formular.update(formular_params)
        format.js { flash.now[:success] = 'Formulaire mis à jour' }
        format.html { redirect_to @formular, notice: 'Formular was successfully updated.' }
        format.json { render :show, status: :ok, location: @formular }
      else
        format.js { flash.now[:danger] = @formular.errors.full_messages.first }
        format.html { render :edit }
        format.json { render json: @formular.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /formulars/1
  # DELETE /formulars/1.json
  def destroy
    @formular.destroy
    respond_to do |format|
      format.js { flash.now[:success] = 'Formulaire supprimé' }
      format.html { redirect_to formulars_url, notice: 'Formulaire supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_formular
      @formular = Formular.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def formular_params
      puts "PARAMS = " + params.to_s
      params.require(:formular).permit(:name, :description, :has_product_categories)
    end
end
