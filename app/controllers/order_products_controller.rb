class OrderProductsController < ApplicationController
  include AccessControl

  before_action :set_order_product, only: [:show, :edit, :update, :destroy,
                                           :increment_quantity, :decrement_quantity]
  before_action :restrict_access_to_connected_users

  # GET /order_products
  # GET /order_products.json
  def index
    @order_products = OrderProduct.all
  end

  # GET /order_products/1
  # GET /order_products/1.json
  def show
  end

  # GET /order_products/new
  def new
    @order_product = OrderProduct.new
  end

  # GET /order_products/1/edit
  def edit
  end

  # POST /order_products
  # POST /order_products.json
  def create
    @order_product = OrderProduct.new(order_product_params)

    respond_to do |format|
      if @order_product.save
        format.js { flash.now[:success] = 'Article ajouté' }
        format.html { redirect_to @order_product, notice: 'Order product was successfully created.' }
        format.json { render :show, status: :created, location: @order_product }
      else
        format.js { flash.now[:error] = @order_product.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @order_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_products/1
  # PATCH/PUT /order_products/1.json
  def update
    respond_to do |format|
      if @order_product.update(order_product_params)
        format.html { redirect_to @order_product, notice: 'Order product was successfully updated.' }
        format.json { render :show, status: :ok, location: @order_product }
      else
        format.html { render :edit }
        format.json { render json: @order_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_products/1
  # DELETE /order_products/1.json
  def destroy
    @order_product.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path, notice: 'Order product was successfully destroyed.' }
      format.json { head :no_content }
      format.js {}
    end
  end

  def increment_quantity
    @order_product.quantity += 1

    if @order_product.save
      respond_to do |format|
        format.js { flash.now[:notice] = 'Quantité augmentée' }
        format.html { redirect_back fallback_location: root_path, notice: 'Quantité augmentée' }
      end
    else
      respond_to do |format|
        format.js { flash.now[:error] = 'Une erreur est survenue' }
        format.html { redirect_back fallback_location: root_path, notice: 'Une erreur est survenue' }
      end
    end
  end

  def decrement_quantity
    if @order_product.quantity == 1
      destroy
    else  
      @order_product.quantity -= 1

      if @order_product.save
        respond_to do |format|
          format.js { flash.now[:notice] = 'Quantité diminuée' }
          format.html { redirect_back fallback_location: root_path, notice: 'Quantité diminuée' }
        end
      else
        respond_to do |format|
          format.js { flash.now[:error] = 'Une erreur est survenue' }
          format.html { redirect_back fallback_location: root_path, notice: 'Une erreur est survenue' }
        end
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_product
      @order_product = OrderProduct.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def order_product_params
      params.require(:order_product).permit(:order_id, :product_id)
    end
end
