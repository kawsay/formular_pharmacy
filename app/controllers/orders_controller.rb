class OrdersController < ApplicationController
  include AccessControl

  before_action :set_formular,        only: [:show, :edit, :new, :create, :update]
  before_action :set_order,           only: [:show, :edit, :new, :update, :destroy]
  before_action :set_order_products,  only: [:edit, :update]
  before_action :build_order_product, only: [:show, :edit, :new]
  before_action :restrict_access_to_connected_users
  before_action :restrict_access_to_administrators_or_concerned_user, only: [:show, :edit]
  before_action :restrict_access_to_administrators, only: [:update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @formular          = Formular.find(order_params[:formular_id])
    @formular_products = @formular.products

    @order             = Order.new(formular_id: @formular.id)
    @order.order_products.build
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(formular_id: @formular.id,
                       barrack:     order_params.dig(:order, :barrack),
                       comment:     order_params.dig(:order, :comment))

    set_order_products

    respond_to do |format|
      if @order.save
        @formular_order = create_formular_order
        create_user_order

        OrderMailer.with(order: @order).new_order_email.deliver_now

        #format.js { flash.now[:success] = 'Soumission prise en compte' }
        format.html { redirect_to [@formular, @order], notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.js { flash.now[:error] = @order.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to [@formular, @order], notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: [@formular, @order] }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = params.has_key?(:id) ? Order.find(params[:id]) : Order.new(order_params)
  end

  def set_formular
    @formular = Formular.find(order_params[:formular_id])
  end

  def set_order_products
    order_products_params = order_params.dig(:order, :order_products_attributes)

    if order_products_params
      order_products_params.each_value do |param|
        unless param['product_id'].blank?
          product = Product.find(param['product_id'])
          @order.order_products.build(product:  product,
                                      quantity: param['quantity'])
        end
      end
    end
  end

  def create_formular_order
    FormularOrder.create!(formular_id: @formular.id, order_id: @order.id)
  end

  def create_user_order
    UserOrder.create(user_id:           current_user.id,
                     order_id:          @order.id,
                     formular_order_id: @formular_order.id)
  end

  def build_order_product
    @builded_order_product = @order.order_products.build
  end

  # Only allow a list of trusted parameters through.
  def order_params
    params.permit(:formular_id,
                  :authenticity_token,
                  :commit,
                  :_method,
                  :id,
                  {
                    order_product:
                    [
                      :id
                    ],
                    order: 
                    [
                      :id,
                      :formular_id,
                      :barrack,
                      :comment,
                      :authenticity_token,
                      :commit,
                      order_products_attributes: OrderProduct.attribute_names.map(&:to_sym).push(:_destroy)
                    ]
                  }
                 )
  end
end
