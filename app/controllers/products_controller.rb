class ProductsController < ApplicationController
  include AccessControl

  before_action :force_json, only: [:search]
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :set_categories, only: [:index, :new, :edit, :update, :destroy]
  before_action :restrict_access_to_administrators

  def search
    q = params[:q].downcase
    @product = Product.where("name LIKE ?", "%#{q}%")
  end

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
    @product  = Product.new
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # PUT/PATCH /add_product_to_category
  # PUT/PATCH /add_product_to_category.json
  def add_to_category
    @product = Product.find_by(name: product_params[:name])

    respond_to do |format|
      if @product.nil?
        format.js { flash.now[:danger] = 'Article inconnu' }
        format.html { redirect_back fallback_location: @product, error: 'Article inconnu' }
        format.json { render status: :not_found }
      elsif @product.product_category_id == product_params[:product_category_id].to_i
        format.js { flash.now[:warning] = 'Déjà dans la catégorie' }
        format.html { redirect_back fallback_location: @product, notice: 'Déjà dans la catégorie' }
        format.json { render :show, status: :not_modified, location: @product }
      elsif @product.update!(product_category_id: product_params[:product_category_id].to_i)
        format.js { flash.now[:success] = 'Article ajouté à la catégorie' }
        format.html { redirect_to @product, notice: 'Article ajouté à la catégorie' }
        format.json { render :show, status: :created, location: @product }
      else
        format.js { flash.now[:error] = @product.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.js { flash.now[:success] = 'Article ajouté' }
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        @categories = ProductCategory.select(:name, :id)
        format.js { flash.now[:error] = @product.errors.full_messages.first }
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.js { flash.now[:success] = 'Article mis à jour' }
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.js { flash.now[:error] = @product.errors.full_messages.first }
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.js { flash.now[:success] = 'Article supprimé' }
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Formce JSON awnser
  def force_json
    request.format = :json
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_categories
    @categories = ProductCategory.select(:name, :id)
  end

  # Only allow a list of trusted parameters through.
  def product_params
    params.require(:product).permit(:name, :product_category_id)
  end
end
