module FlashHelper
  # Returns the CSS classes depending on the flash' key
  def flash_class(key)
    case key.to_sym
      when :success then "alert alert-success"
      when :danger then "alert alert-danger"
      when :error then "alert alert-danger"
      when :alert then "alert alert-warning"
      when :warning then "alert alert-warning"
      else "alert alert-info"
    end
  end

  # Generates the flash' HTML content
  def display_flash(key, value)
    div_class = [flash_class(key), 'text-center fade show alert-dismissible'].join(' ')

    content_tag(:div, class: div_class) do
      content_tag(:p, value) +
      button_tag(class: 'ml-auto close', 'data-dismiss': 'alert', type: 'button') do
        content_tag(:span, '&times;'.html_safe, 'aria-hidden': 'true') +
        content_tag(:span, 'Close', class: 'sr-only')
      end
    end
  end
end

