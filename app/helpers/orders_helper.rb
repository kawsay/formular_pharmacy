module OrdersHelper
  def build_table(formular, order, f)
    header = formular.has_product_categories ? render_categorized_header : render_default_header

#    if formular.has_product_categories && formular.products_have_dosage
#      body = render_categorized_body_with_dosage(formular, order, f)
#    elsif formular.has_product_categories && !formular.products_have_dosage
#      body = render_categorized_body(formular, order, f)
#    else
#      body = render_default_body(formular, order, f)
#    end

    #header + body
    header
  end

  def render_default_header
    render partial: 'orders/form_table_default_header'
  end

  def render_categorized_header
    render partial: 'orders/form_table_categorized_header'
  end

  def render_default_body(formular, order, f)
    render partial: 'orders/form_table_default_body', locals: { formular: formular, order: order, f: f }
  end

  def render_categorized_body(formular, order, f)
    render partial: 'orders/form_table_categorized_body', locals: { formular: formular, order: order, f: f }
  end

  def render_categorized_body_with_dosage(formular, order, f)
    render partial: 'orders/form_table_categorized_body_with_dosage', locals: { formular: formular, order: order, f: f }
  end
end
