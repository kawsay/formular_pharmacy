// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

import 'bootstrap'
import "@fortawesome/fontawesome-free/js/all"
require("easy-autocomplete")
window.jQuery = $;
window.$ = $;

import 'packs/products'
require("packs/custom/flash")
require("packs/custom/session")
require("packs/custom/add_product_row")
require("packs/custom/delete_product_row")
require("packs/custom/order_products")

Rails.start()
Turbolinks.start()
ActiveStorage.start()
