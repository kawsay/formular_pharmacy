$(document).on('turbolinks:load', function() {
  $('form').on('click', '.remove_order_product', function(event) {
    $(this).prev('input[type=hidden]').val('1');
    $(this).closest('tr').hide();
    return event.preventDefault();
  });
});
