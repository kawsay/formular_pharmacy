window.closeFlash = function() {
  let lastAlert = $('#flash .alert:last')

  function fadeFlash() {
    lastAlert.animate( {opacity: 0}, 5000, function() {
      $(this).hide('slow', function() {
        $(this).remove()
      });
    });
  };

setTimeout(fadeFlash, 8000)
};

