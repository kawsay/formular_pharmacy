// TODO: update fields when cat changes
// Once turbolinks is loaded, create the following function.
document.addEventListener('turbolinks:load', function() {
  // filterOrderProductCategories is called each time a row is added
  window.filterOrderProductCategories = function() {
    var order_product_rows = $('.order_product_row');

    // When a category is selected, set products which belongs to this category
    //
    // Iterate rows
    order_product_rows.each(function(index, element) {
      // When a changed is noticed
      $(element).find('select:first').change(function() {
        // Fetch the selected category, the products' <select> tag and the options
        // corresponding to the selected category
        var category = $(element).find('select:first option:selected').text();
        var products = $(element).find('select:last');
        var options  = $(element).find('optgroup[label="' + category +'"]').html();

        // If a category is selected ('Categorie...' has no options)
        if (options) {
          // Set the corresponding products' <option>s
          $(products).html(options);
        };
      });
    });
  };
});
