document.addEventListener("turbolinks:load", function() {
  console.log('products');
  $input = $('*[data-behavior="autocomplete"]')

  var options = {
    url: function(q) {
      return "/product/search.json?q=" + q;
    },
    getValue: "name",
  };

  $input.easyAutocomplete(options);
});
