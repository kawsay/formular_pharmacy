class ApplicationMailer < ActionMailer::Base
  default from: 'domaines-foncionnels@sdis21.org'
  layout 'mailer'
end
