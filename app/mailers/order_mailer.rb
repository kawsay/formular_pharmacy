class OrderMailer < ApplicationMailer
  def new_order_email
    @formular_order = FormularOrder.find_by(order_id: params[:order])
    @formular       = @formular_order.formular

    mail to: 'pharmacie@sdis21.org', subject: 'Nouvelle commande'
  end
end
