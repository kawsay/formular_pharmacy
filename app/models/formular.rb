class Formular < ApplicationRecord
  # Order & Formular jointable
  has_many :formular_orders, dependent: :delete_all
  has_many :submissions, foreign_key: 'formular_id', class_name: 'FormularOrder', dependent: :destroy

  # Formular & Order relationship
  has_many :orders, dependent: :destroy

  # Product & Formular jointable
  has_many :formular_products, dependent: :destroy
  has_many :products, through: :formular_products

  # Data validation
  validates :name,
    presence: true,
    uniqueness: true,
    length: {
      minimum:   2,
      maximum:   128,
      too_short: "Nom du formulaire trop court, %{count} caractères minimum",
      too_long:  "Nom du formulaire trop long, %{count} caractères maximum"
    }

  def products_names
    products.map(&:name)
  end

  def submissions
    formular_orders.order(created_at: :desc)
  end
end
