class FormularOrder < ApplicationRecord
  belongs_to :formular
  belongs_to :order

  # OrderProduct relationship
  has_many :order_products, through: :order

  # User relationship
  has_one :user_order
  has_one :user, through: :user_order

  def submited_at
    created_at.strftime('%d/%m/%Y à %Hh%M')
  end

  def products
    order.products
  end

  def barrack
    order.barrack
  end

  def comment
    order.comment
  end
end
