class Order < ApplicationRecord
  attr_accessor :authenticity_token, :commit, :_method, :order
  # Order & FormularOrder jointable
  has_many :formular_order, dependent: :delete_all
  has_many :submissions,
          foreign_key: 'order_id',
          class_name: 'FormularOrder'

  # Order & User jointable
  has_many :user_orders, dependent: :delete_all
  has_many :users, through: :user_orders

  # Order & Formular relationship
  belongs_to :formular
  accepts_nested_attributes_for :formular

  # Order & Product jointable
  has_many :order_products, dependent: :delete_all
  has_many :products, through: :order_products
  accepts_nested_attributes_for :order_products,
                                allow_destroy: true,
                                reject_if: proc { |attr| attr.order_product.product['name'].blank? }
end
