class OrderProduct < ApplicationRecord
  belongs_to :order, optional: true
  belongs_to :product
  has_one :category, through: :product

  def product_name
    product&.name || ''
  end
end
