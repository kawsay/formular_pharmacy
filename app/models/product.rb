class Product < ApplicationRecord
  # Product & Formular jointable
  has_many :formular_product
  has_many :formulars, through: :formular_product

  # Product & Order jointable
  has_many :order_product
  has_many :orders, through: :order_product

  # Product's category relation
  belongs_to :category, foreign_key: 'product_category_id', class_name: 'ProductCategory', optional: true

  # Data validation
  validates :name,
    presence: true,
    uniqueness: true,
    length: {
      minimum: 2,
      maximum: 256,
      too_short: "Nom du produit trop court, %{%count} caractères minimum",
      too_long: "Nom du produit trop long, %{%count} caractères minimum"
    }
end
