class User < ApplicationRecord
  ADMINISTRATOR_CN =
    [
      ENV['FORM_PHARMACIE_GSIC_ADMINISTRATORS_CN'],
      ENV['FORM_PHARMACIE_SSSM_ADMINISTRATORS_CN']
  ]

  # Set remember_token
  before_create :remember_value
  before_validation :set_is_admin?

  # Include default devise modules.
  devise :ldap_authenticatable, :rememberable, :trackable

  # User & Order jointable
  has_many :user_order
  has_many :orders, through: :user_order

  # User & Product jointable
  has_many :order_products, through: :orders
  has_many :products, through: :order_products

  # User's remembered barrack
  enum barrack:
    [
      :"Aiserey",
      :"Aisey-sur-Seine",
      :"Arc-sur-Tille / Remilly-sur-Tille",
      :"Arnay-le-Duc",
      :"Athée",
      :"Auxonne",
      :"Baigneux-les-Juifs",
      :"Beaune",
      :"Blaisy-Bas",
      :"Bligny-sur-Ouche",
      :"Brazey-en-Plaine",
      :"Bretigny",
      :"Broin/Bonnencontre",
      :"Chanceaux",
      :"Chatillon-sur-Seine",
      :"Corberon / Corgengoux",
      :"Darcey",
      :"Dijon Est",
      :"Dijon Nord",
      :"Dijon Transvaal",
      :"Fontaine-Francaise",
      :"Genlis",
      :"Gevrey-Chambertin",
      :"Grancey-le-Chateau",
      :"Aignay-le-Duc",
      :"Is-sur-Tille",
      :"Jailly-les-moulins / Villy-en-Auxois",
      :"Lacanche",
      :"Laignes",
      :"Lamarche-sur-Saone",
      :"Les deux côtes",
      :"Liernais",
      :"Losne",
      :"Les-Maillys",
      :"Marsannay-le-Bois",
      :"Merceuil",
      :"Meursault",
      :"Mirebeau-sur-Bèze",
      :"Montbard",
      :"Montigny-sur-Aube",
      :"Morey-Saint-Denis / Chambolle-Musigny",
      :"Nolay",
      :"Nuits-Saint-Georges",
      :"Pagny-le-Château / Pagny-la-ville",
      :"Perrigny-sur-l'Ognon",
      :"Pontailler-sur-Saône",
      :"Pouilly-en-Auxois",
      :"Precy-sous-Thil",
      :"Quincey",
      :"Recey-sur-Ource / Leuglay-Voulaines",
      :"Rouvray",
      :"Ruffey-les-Beaune",
      :"Saint-Jean-de-Losne",
      :"Saint-Julien",
      :"Saint-Seine-l'abbaye",
      :"Saint-Seine-sur-Vingeanne",
      :"Santenay",
      :"Saulieu",
      :"Saulon-la-chapelle",
      :"Selongey",
      :"Semur-en-Auxois",
      :"Seurre",
      :"Sombernon",
      :"Thoisy-la-Berchère",
      :"Thury",
      :"Til-Châtel",
      :"Toutry",
      :"Trouhans",
      :"Val d'Ouche",
      :"Venarey-les-Laumes",
      :"Vitteaux"
  ]

  private

  def remember_value
    self.remember_token ||= Devise.friendly_token
  end

  def set_is_admin?
    # Useful for development environment, as usernames come from Faker
    return unless valid_ldap_login
    # Might be useful for development environment too, for hardcoded Users in db/seed.rb
    return unless self.is_admin? == nil

    administrator_membership = ADMINISTRATOR_CN & Devise::LDAP::Adapter.get_ldap_param(self.username, 'memberOf')

    if administrator_membership.empty?
      self.update_attribute(:is_admin?, false)
    else
      self.update_attribute(:is_admin?, true)
    end
  end

  def valid_ldap_login
    Devise::LDAP::Adapter.valid_login?(self.username)
  end
end
