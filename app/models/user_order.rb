class UserOrder < ApplicationRecord
  belongs_to :user
  belongs_to :order
  belongs_to :formular_order
end
