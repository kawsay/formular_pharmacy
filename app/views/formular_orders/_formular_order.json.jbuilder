json.extract! formular_order, :id, :formular_id, :order_id, :created_at, :updated_at
json.url formular_order_url(formular_order, format: :json)
