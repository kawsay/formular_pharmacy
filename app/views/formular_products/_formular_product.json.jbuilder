json.extract! formular_product, :id, :formular_id, :product_id, :created_at, :updated_at
json.url formular_product_url(formular_product, format: :json)
