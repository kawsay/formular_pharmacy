json.extract! formular, :id, :name, :created_at, :updated_at
json.url formular_url(formular, format: :json)
