Rails.application.routes.draw do
  resources :user_orders, :products, :product_categories

  resources :formulars do
    resources :formular_products
    resources :formular_orders
    resources :orders do
      resources :order_products, shallow: true
    end
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }

  devise_scope :user do
    root to: 'users/sessions#new'
    get 'sign_in', to: 'users/sessions#new'
    get '/users/sign_out', to: 'users/sessions#destroy'
  end

  match 'add_product_to_category', to: 'products#add_to_category', via: [:patch, :put]
  get 'home', to: 'formulars#index'
  get 'product/search', to: 'products#search'

  patch 'formulars/:formular_id/orders/:order_id/order_products/:id/increment_order_product_quantity',
    to: 'order_products#increment_quantity',
    as: 'increment_order_product_quantity'

  patch 'formulars/:formular_id/orders/:order_id/order_products/:id/decrement_order_product_quantity',
    to: 'order_products#decrement_quantity',
    as: 'decrement_order_product_quantity'

  get 'formular_order_modal/:id', to: 'formular_orders#modal', as: 'formular_order_modal'
end
