class CreateFormulars < ActiveRecord::Migration[6.1]
  def change
    create_table :formulars do |t|
      t.string :name

      t.timestamps
    end
  end
end
