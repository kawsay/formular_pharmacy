class CreateFormularOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :formular_orders do |t|
      t.references :formular, null: false, foreign_key: true
      t.references :order, null: false, foreign_key: true

      t.timestamps
    end
  end
end
