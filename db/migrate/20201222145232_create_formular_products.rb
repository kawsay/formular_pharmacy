class CreateFormularProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :formular_products do |t|
      t.references :formular, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
