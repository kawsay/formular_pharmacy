class AddDescriptionToFormulars < ActiveRecord::Migration[6.1]
  def change
    add_column :formulars, :description, :text
  end
end
