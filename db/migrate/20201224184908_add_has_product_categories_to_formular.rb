class AddHasProductCategoriesToFormular < ActiveRecord::Migration[6.1]
  def change
    add_column :formulars, :has_product_categories?, :boolean, default: false, required: true
  end
end
