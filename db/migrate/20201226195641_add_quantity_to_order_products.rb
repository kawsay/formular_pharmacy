class AddQuantityToOrderProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :order_products, :quantity, :integer, default: 1, required: true
  end
end
