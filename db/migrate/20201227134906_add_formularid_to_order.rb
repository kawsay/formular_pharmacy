class AddFormularidToOrder < ActiveRecord::Migration[6.1]
  def change
    add_reference :orders, :formular, null: false, foreign_key: true, default: 0
  end
end
