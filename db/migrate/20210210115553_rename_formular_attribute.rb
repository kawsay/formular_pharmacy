class RenameFormularAttribute < ActiveRecord::Migration[6.1]
  def change
    rename_column :formulars, :has_product_categories?, :has_product_categories
  end
end
