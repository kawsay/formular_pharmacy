class AddFormularOrdersReferenceToUserOrders < ActiveRecord::Migration[6.1]
  def change
    add_reference :user_orders, :formular_order, index: true
  end
end
