class ChangeIsAdminDefaultValueToUser < ActiveRecord::Migration[6.1]
  def change
    change_column_default :users, :is_admin?, from: false, to: nil
  end
end
