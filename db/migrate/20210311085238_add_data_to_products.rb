class AddDataToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :description, :string
    add_column :products, :dosage, :string
    add_column :products, :maximum_quantity, :integer
    add_column :products, :minimum_quantity, :integer
  end
end
