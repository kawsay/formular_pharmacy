class AddProductsHaveDosageToFormulars < ActiveRecord::Migration[6.1]
  def change
    add_column :formulars, :products_have_dosage, :boolean
  end
end
