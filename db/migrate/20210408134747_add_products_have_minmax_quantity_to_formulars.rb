class AddProductsHaveMinmaxQuantityToFormulars < ActiveRecord::Migration[6.1]
  def change
    add_column :formulars, :products_have_minimum_quantity, :boolean
    add_column :formulars, :products_have_maximum_quantity, :boolean
  end
end
