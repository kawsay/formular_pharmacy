class RemoveMinimumQuantityAndMaximumQuantityToProducts < ActiveRecord::Migration[6.1]
  def change
    remove_column :products, :maximum_quantity
    remove_column :products, :minimum_quantity
  end
end
