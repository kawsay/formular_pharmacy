class AddMinmaxQuantityToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :minimum_quantity, :string
    add_column :products, :maximum_quantity, :string
  end
end
