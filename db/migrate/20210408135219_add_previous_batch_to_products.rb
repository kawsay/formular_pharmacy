class AddPreviousBatchToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :previous_batch, :string
  end
end
