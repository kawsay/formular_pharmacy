class AddProductsHavePreviousBatchToFormulars < ActiveRecord::Migration[6.1]
  def change
    add_column :formulars, :products_have_previous_batch, :boolean
  end
end
