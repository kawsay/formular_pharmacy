class AddDefaultValueToFormularAttributes < ActiveRecord::Migration[6.1]
  def change
    change_column :formulars, :products_have_dosage,           :boolean, default: false
    change_column :formulars, :products_have_minimum_quantity, :boolean, default: false
    change_column :formulars, :products_have_maximum_quantity, :boolean, default: false
    change_column :formulars, :products_have_previous_batch,   :boolean, default: false
  end
end
