# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_08_141841) do

  create_table "formular_orders", force: :cascade do |t|
    t.integer "formular_id", null: false
    t.integer "order_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["formular_id"], name: "index_formular_orders_on_formular_id"
    t.index ["order_id"], name: "index_formular_orders_on_order_id"
  end

  create_table "formular_products", force: :cascade do |t|
    t.integer "formular_id", null: false
    t.integer "product_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["formular_id"], name: "index_formular_products_on_formular_id"
    t.index ["product_id"], name: "index_formular_products_on_product_id"
  end

  create_table "formulars", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "description"
    t.boolean "has_product_categories", default: false
    t.boolean "products_have_dosage", default: false
    t.boolean "products_have_minimum_quantity", default: false
    t.boolean "products_have_maximum_quantity", default: false
    t.boolean "products_have_previous_batch", default: false
  end

  create_table "order_products", force: :cascade do |t|
    t.integer "order_id", null: false
    t.integer "product_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "quantity", default: 1
    t.index ["order_id"], name: "index_order_products_on_order_id"
    t.index ["product_id"], name: "index_order_products_on_product_id"
  end

  create_table "orders", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "formular_id", default: 0, null: false
    t.string "barrack"
    t.text "comment"
    t.index ["formular_id"], name: "index_orders_on_formular_id"
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.integer "product_category_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.string "dosage"
    t.string "minimum_quantity"
    t.string "maximum_quantity"
    t.string "previous_batch"
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
  end

  create_table "user_orders", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "order_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "formular_order_id"
    t.index ["formular_order_id"], name: "index_user_orders_on_formular_order_id"
    t.index ["order_id"], name: "index_user_orders_on_order_id"
    t.index ["user_id"], name: "index_user_orders_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_admin?"
    t.integer "barrack"
    t.string "remember_token"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "formular_orders", "formulars"
  add_foreign_key "formular_orders", "orders"
  add_foreign_key "formular_products", "formulars"
  add_foreign_key "formular_products", "products"
  add_foreign_key "order_products", "orders"
  add_foreign_key "order_products", "products"
  add_foreign_key "orders", "formulars"
  add_foreign_key "products", "product_categories"
  add_foreign_key "user_orders", "orders"
  add_foreign_key "user_orders", "users"
end
