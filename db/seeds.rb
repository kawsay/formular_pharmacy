# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

# Load libraries & CSV files
require 'csv'
require 'faker'
ampoulier_inf = CSV.readlines('resources/FORM_PUI_AMPOULIER_INF.csv')[1..-1]
ampoulier     = CSV.readlines('resources/FORM_PUI_AMPOULIER.csv')[1..-1]
vsav          = CSV.readlines('resources/FORM_PUI_VSAV.csv')[1..-1]
puts 'resources & dependencies loaded'


# Delete from all tables
[
  OrderProduct, FormularOrder, UserOrder, Order, FormularProduct, User,
  FormularProduct, Formular, Product, ProductCategory
].each(&:destroy_all)
puts ":destroy_all"


# Create Formular instances
formulars = [
  # [
  #   0 name:string
  #   1 has_product_categories:boolean
  #   2 products_have_minimum_quantity:boolean
  #   3 products_have_maximum_quantity:boolean
  #   4 products_have_dosage:boolean
  #   5 products_have_previous_batch:boolean
  # ]
  ['Formulaire PUI Ampoulier', false, true, true, true, true],
  ['Formulaire PUI Ampoulier INF', false, true, true, true, true],
  ['Formulaire PUI VSAV', true, false, false, false, false]
]

3.times do |i|
  Formular.create!(
    name:        formulars[i][0],
    description: Faker::Lorem.words(number: 15).join(' ').capitalize,
    has_product_categories:         formulars[i][1],
    products_have_minimum_quantity: formulars[i][2],
    products_have_maximum_quantity: formulars[i][3],
    products_have_dosage:           formulars[i][4],
    products_have_previous_batch:   formulars[i][5]
  )
end
puts "Formular"

# Create ProductCategory instances
categories_names = vsav.map(&:last).uniq
categories_names.each do |category_name|
  ProductCategory.create!(
    name: category_name
  )
end
puts "ProductCategory"


# Create Product instances
puts "About to insert #{vsav.size} rows"
vsav.each do |row|
  product = Product.create!(
    name: row[0],
    product_category_id: ProductCategory.find_by_name(row[1]).id
  )
  FormularProduct.create!(
    formular_id: Formular.find_by_name('Formulaire PUI VSAV').id,
    product_id:  product.id
  )
end
puts "#{Product.all.size} rows inserted"
ampoulier.each do |row|
  product = Product.create!(
    name:             row[0],
    dosage:           row[1],
    description:      row[2],
    maximum_quantity: row[3],
    minimum_quantity: row[4]
  )
  FormularProduct.create!(
    formular_id: Formular.find_by_name('Formulaire PUI Ampoulier').id,
    product_id:  product.id
  )
end
ampoulier_inf.each do |row|
  next unless Product.find_by_name(row[0]).nil?
  product = Product.create!(
    name:             row[0],
    dosage:           row[1],
    description:      row[2],
    maximum_quantity: row[3],
    minimum_quantity: row[4]
  )
  FormularProduct.create!(
    formular_id: Formular.find_by_name('Formulaire PUI Ampoulier INF').id,
    product_id:  product.id
  )
end
puts "Product"

10.times do
  User.create!(
    username: Faker::Name.name,
    barrack: [0, 1].sample,
    is_admin?: false
  )
end
User.create!(
  username: 'foo',
  password: 'bar',
  is_admin?: true
)
puts "User"

15.times do |n|
  order = Order.create!(
    formular_id: Formular.all.sample.id,
    barrack:     User.barracks.keys.sample
  )
  puts "Order" if n == 0

  formular_order = FormularOrder.create!(
    formular_id: order.formular_id,
    order_id: order.id
  )
  puts "FormularOrder" if n == 0

  UserOrder.create!(
    user_id: User.all.sample.id,
    order_id: order.id,
    formular_order_id: formular_order.id
  )
  puts "UserOrder" if n == 0

  rand(8).times do |j|
    OrderProduct.create!(
      order_id: order.id,
      product_id: Product.all.sample.id
    )
    puts "OrderProduct" if n == 0 && j == 0
  end
end

