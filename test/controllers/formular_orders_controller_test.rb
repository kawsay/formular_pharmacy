require "test_helper"

class FormularOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @formular_order = formular_orders(:one)
  end

  test "should get index" do
    get formular_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_formular_order_url
    assert_response :success
  end

  test "should create formular_order" do
    assert_difference('FormularOrder.count') do
      post formular_orders_url, params: { formular_order: { formular_id: @formular_order.formular_id, order_id: @formular_order.order_id } }
    end

    assert_redirected_to formular_order_url(FormularOrder.last)
  end

  test "should show formular_order" do
    get formular_order_url(@formular_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_formular_order_url(@formular_order)
    assert_response :success
  end

  test "should update formular_order" do
    patch formular_order_url(@formular_order), params: { formular_order: { formular_id: @formular_order.formular_id, order_id: @formular_order.order_id } }
    assert_redirected_to formular_order_url(@formular_order)
  end

  test "should destroy formular_order" do
    assert_difference('FormularOrder.count', -1) do
      delete formular_order_url(@formular_order)
    end

    assert_redirected_to formular_orders_url
  end
end
