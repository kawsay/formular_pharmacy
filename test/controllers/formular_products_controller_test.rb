require "test_helper"

class FormularProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @formular_product = formular_products(:one)
  end

  test "should get index" do
    get formular_products_url
    assert_response :success
  end

  test "should get new" do
    get new_formular_product_url
    assert_response :success
  end

  test "should create formular_product" do
    assert_difference('FormularProduct.count') do
      post formular_products_url, params: { formular_product: { formular_id: @formular_product.formular_id, product_id: @formular_product.product_id } }
    end

    assert_redirected_to formular_product_url(FormularProduct.last)
  end

  test "should show formular_product" do
    get formular_product_url(@formular_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_formular_product_url(@formular_product)
    assert_response :success
  end

  test "should update formular_product" do
    patch formular_product_url(@formular_product), params: { formular_product: { formular_id: @formular_product.formular_id, product_id: @formular_product.product_id } }
    assert_redirected_to formular_product_url(@formular_product)
  end

  test "should destroy formular_product" do
    assert_difference('FormularProduct.count', -1) do
      delete formular_product_url(@formular_product)
    end

    assert_redirected_to formular_products_url
  end
end
