require "test_helper"

class FormularsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @formular = formulars(:one)
  end

  test "should get index" do
    get formulars_url
    assert_response :success
  end

  test "should get new" do
    get new_formular_url
    assert_response :success
  end

  test "should create formular" do
    assert_difference('Formular.count') do
      post formulars_url, params: { formular: { name: @formular.name } }
    end

    assert_redirected_to formular_url(Formular.last)
  end

  test "should show formular" do
    get formular_url(@formular)
    assert_response :success
  end

  test "should get edit" do
    get edit_formular_url(@formular)
    assert_response :success
  end

  test "should update formular" do
    patch formular_url(@formular), params: { formular: { name: @formular.name } }
    assert_redirected_to formular_url(@formular)
  end

  test "should destroy formular" do
    assert_difference('Formular.count', -1) do
      delete formular_url(@formular)
    end

    assert_redirected_to formulars_url
  end
end
