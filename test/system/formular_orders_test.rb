require "application_system_test_case"

class FormularOrdersTest < ApplicationSystemTestCase
  setup do
    @formular_order = formular_orders(:one)
  end

  test "visiting the index" do
    visit formular_orders_url
    assert_selector "h1", text: "Formular Orders"
  end

  test "creating a Formular order" do
    visit formular_orders_url
    click_on "New Formular Order"

    fill_in "Formular", with: @formular_order.formular_id
    fill_in "Order", with: @formular_order.order_id
    click_on "Create Formular order"

    assert_text "Formular order was successfully created"
    click_on "Back"
  end

  test "updating a Formular order" do
    visit formular_orders_url
    click_on "Edit", match: :first

    fill_in "Formular", with: @formular_order.formular_id
    fill_in "Order", with: @formular_order.order_id
    click_on "Update Formular order"

    assert_text "Formular order was successfully updated"
    click_on "Back"
  end

  test "destroying a Formular order" do
    visit formular_orders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Formular order was successfully destroyed"
  end
end
