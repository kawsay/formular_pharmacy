require "application_system_test_case"

class FormularProductsTest < ApplicationSystemTestCase
  setup do
    @formular_product = formular_products(:one)
  end

  test "visiting the index" do
    visit formular_products_url
    assert_selector "h1", text: "Formular Products"
  end

  test "creating a Formular product" do
    visit formular_products_url
    click_on "New Formular Product"

    fill_in "Formular", with: @formular_product.formular_id
    fill_in "Product", with: @formular_product.product_id
    click_on "Create Formular product"

    assert_text "Formular product was successfully created"
    click_on "Back"
  end

  test "updating a Formular product" do
    visit formular_products_url
    click_on "Edit", match: :first

    fill_in "Formular", with: @formular_product.formular_id
    fill_in "Product", with: @formular_product.product_id
    click_on "Update Formular product"

    assert_text "Formular product was successfully updated"
    click_on "Back"
  end

  test "destroying a Formular product" do
    visit formular_products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Formular product was successfully destroyed"
  end
end
