require "application_system_test_case"

class FormularsTest < ApplicationSystemTestCase
  setup do
    @formular = formulars(:one)
  end

  test "visiting the index" do
    visit formulars_url
    assert_selector "h1", text: "Formulars"
  end

  test "creating a Formular" do
    visit formulars_url
    click_on "New Formular"

    fill_in "Name", with: @formular.name
    click_on "Create Formular"

    assert_text "Formular was successfully created"
    click_on "Back"
  end

  test "updating a Formular" do
    visit formulars_url
    click_on "Edit", match: :first

    fill_in "Name", with: @formular.name
    click_on "Update Formular"

    assert_text "Formular was successfully updated"
    click_on "Back"
  end

  test "destroying a Formular" do
    visit formulars_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Formular was successfully destroyed"
  end
end
